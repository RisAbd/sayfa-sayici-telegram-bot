#!/bin/bash

commands='\
cd sayfa-sayici-telegram-bot/ \
&& pwd \
&& ( set -o allexport &&  source .env; set +o allexport ) \
&& export GIT_SSH_COMMAND="ssh -i ~/.ssh/gitlab-ci_id_rsa" \
&& git pull \
&& git submodule update \
&& /home/pi/.local/bin/pipenv run flask db upgrade \
&& systemctl --user restart sayfasayicibot.service \
&& echo restarted
'

echo "$commands" | ssh -T pi@vc-emed-gov.com.kg -p 2222 -o StrictHostKeyChecking=no
