"""empty message

Revision ID: dbf2043ea5f7
Revises: ee4590a0e808
Create Date: 2024-10-04 21:57:46.479607

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dbf2043ea5f7'
down_revision = 'ee4590a0e808'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user_settings', schema=None) as batch_op:
        batch_op.add_column(sa.Column('timezone', sa.String(), nullable=True))
        batch_op.add_column(sa.Column('daily_checkpoint_time', sa.Time(), nullable=False))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user_settings', schema=None) as batch_op:
        batch_op.drop_column('daily_checkpoint_time')
        batch_op.drop_column('timezone')

    # ### end Alembic commands ###
