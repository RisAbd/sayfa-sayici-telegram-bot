
import itertools as IT
import re
import time

from jinja2 import Environment
from sqlalchemy import func

from telegram.telegram import Bot, Update, Chat, Message, InlineKeyboardMarkup
import models
from datetime import datetime, timedelta
from sqlalchemy.orm import joinedload, Session
from long_poll_updates import BOOK_CMD, CALLBACK_DATA_CMD_PREFIX

env = Environment()
env.globals['now'] = datetime.now()
env.filters['cur_year_date_format'] = lambda dt: dt.strftime("%d/%m/%Y %H:%M:%S"
                                                             if dt.year != datetime.now().year
                                                             else "%d/%m %H:%M:%S")

_stats_text_template = env.from_string('''
{{- custom_msg or '' }}

you have read
{%- if today_sayfa is not none %}
 - `{{ today_sayfa }}` sayfa from {{ daily_checkpoint_time.strftime('%H:%M') }}
{%- endif %}
 - `{{ last_day_sayfa }}` sayfa for last 24 hours
 - `{{ last_week_sayfa }}` sayfa for last week
 - `{{ last_month_sayfa }}` sayfa for last month
{%- if last_checkpoint is not none %}
 - `{{ last_checkpoint_sayfa }}` sayfa from last checkpoint _({{ in_user_timezone(last_checkpoint_time) | cur_year_date_format }})_
{%- endif -%}
''')  # noqa: E501

_sayfa_list_template = env.from_string('''
{%- if last_checkpoint -%}
`{{ in_user_timezone(last_checkpoint_time) | cur_year_date_format }}` - last checkpoint
{%- endif %}

{% for s in sayfalar -%}
{%- if loop.previtem and in_user_timezone(loop.previtem.time).date() != in_user_timezone(s.time).date() -%}
------------------------------
{% endif -%}
`{{ in_user_timezone(s.time) | cur_year_date_format }}` - *{{ s.count }}* - {{ s.book.title }}
{% else -%}
_you have not read any sayfa yet_
{%- endfor %}
''')


def save_user_book(bot: Bot, update: Update, bot_command: str, session: Session):
    user = models.User.get_or_create(session, update.callback_query.from_)

    book_id_raw = bot_command.lstrip(BOOK_CMD)
    try:
        book_id = int(book_id_raw)
    except ValueError:
        return bot.send_message(
            chat=update.callback_query.message.chat,
            text="unknown book: %s" % book_id_raw,
        )

    book = session.query(models.Book).get(book_id)
    if not book:
        return bot.send_message(
            chat=update.callback_query.message.chat,
            text="unknown book: %s" % book_id,
        )

    if book not in user.books:
        user.books.append(book)
        is_added = True
    else:
        user.books.remove(book)
        is_added = False

    new_books_list = user.books

    session.add(user)
    session.commit()

    msg = update.callback_query.message

    buttons = IT.chain.from_iterable(
        update.callback_query.message.reply_markup.inline_keyboard
    )
    book_ids = [int(b.callback_data.rsplit('_', 1)[1]) for b in buttons]
    books_from_markup = session.query(models.Book) \
        .filter(models.Book.id.in_(book_ids)) \
        .all()
    books_from_markup.sort(key=lambda b: book_ids.index(b.id))

    bot.edit_message_reply_markup(
        chat=msg.chat, message=msg,
        markup=_books_markup(session, user=user, books=books_from_markup),
    )

    if is_added and len(new_books_list) == 1:
        text = "`%s` is set as your default book" % book.title
    elif is_added:
        text = "`%s` added to your books list" % book.title
    else:
        text = "`%s` is removed from your books list" % book.title

    return bot.answer_callback_query(
        update.callback_query, text=text,
    )


def user_checkpoint(bot: Bot, update: Update, session: Session):
    user = models.User.get_or_create(session, update.message.from_)

    name = update.message.bot_command_argument.strip() or None

    exists_q = (session.query(models.Checkpoint)
                .filter(models.Checkpoint.user_id == user.id).exists())
    prev_checkpoint_exists = session.query(exists_q).scalar()

    user_stats_before_checkpoint = _user_stats(bot, update, session, user=user)

    checkpoint = models.Checkpoint(user=user, name=name, time=update.message.date)
    session.add(checkpoint)
    session.commit()

    if prev_checkpoint_exists:
        text = "new checkpoint created: %s"
    else:
        text = "you created your first checkpoint: %s"

    bot.send_message(
        chat=update.message.chat,
        text=text % (checkpoint),
    )

    return user_stats_before_checkpoint


def user_books(bot: Bot, update: Update, session: Session):
    user = models.User.get_or_create(session, update.message.from_)

    if not user.books:
        bot.send_message(
            chat=update.message.chat,
            text="you haven't set your books yet",
        )
        return send_books_list(bot, update, session)

    markup = _books_markup(session, user=user, books=user.books)

    return bot.send_message(
        chat=update.message.chat,
        text="your selected books",
        reply_markup=markup,
    )


def save_bot_command_sayfa(bot: Bot, update: Update, bot_command: str, session: Session):
    try:
        _, book_id, sayfa_count = bot_command.split()
        book_id = int(book_id)
        sayfa_count = int(sayfa_count)
    except (ValueError, TypeError):
        return bot.send_message(
            chat=update.callback_query.message.chat,
            text="misunderstood sayfa value: `%s`" % bot_command[6:],
            parse_mode=Message.ParseMode.MARKDOWN,
        )

    user = models.User.get_or_create(session, update.callback_query.from_)
    book = session.query(models.Book).get(book_id)
    if not book:
        return bot.send_message(
            chat=update.callback_query.message.chat,
            text="unknown book: %s" % book_id,
        )

    sayfa = models.Sayfa(user=user, book=book, count=sayfa_count,
                         time=update.callback_query.message.date)
    session.add(sayfa)
    session.commit()

    text = _get_main_sayfa_text(sayfa, user=user)

    bot.answer_callback_query(
        update.callback_query,
        text=text,
    )
    return _user_stats(bot, update, session, user=user,
                       with_message=text,
                       edit_message=update.callback_query.message,
                       )


def user_sayfa(bot: Bot, update: Update, session):
    user = models.User.get_or_create(session, update.message.from_)

    q = session.query(models.Sayfa) \
        .options(joinedload(models.Sayfa.book)) \
        .filter(models.Sayfa.user == user)

    checkpoint = session.query(models.Checkpoint) \
        .filter(models.Checkpoint.user == user) \
        .order_by(models.Checkpoint.time.desc()) \
        .first()

    if checkpoint:
        q = q.filter(models.Sayfa.time > checkpoint.time)

    text = _sayfa_list_template.render(
        last_checkpoint=checkpoint,
        last_checkpoint_time=checkpoint and checkpoint.time,
        sayfalar=q,
        in_user_timezone=user.settings.as_in_user_timezone,
    )

    return bot.send_message(
        chat=update.message.chat,
        text=text,
        parse_mode=Message.ParseMode.MARKDOWN,
    )


def user_stats(bot: Bot, update: Update, session: Session, user=None, with_message=None):
    return _user_stats(bot=bot, update=update, session=session, user=user, with_message=with_message)


def save_user(bot: Bot, update: Update, session: Session):
    user, is_created = models.User.get_or_create(
        session, update.message.from_, _flag=True, _update=True
    )
    return bot.send_message(
        chat=update.message.chat,
        text="Welcome{}, {}".format("" if is_created else " back", user.full_name),
    )


def send_books_list(bot: Bot, update: Update, session: Session):
    user = models.User.get_or_create(session, update.message.from_)

    markup = _books_markup(session, user=user)

    return bot.send_message(
        chat=update.message.chat,
        text="here is a list of available books",
        reply_markup=markup,
    )


def send_audio(bot: Bot, update: Update):
    bot.send_chat_action(update.message.chat, Chat.Action.UPLOAD_AUDIO)
    time.sleep(0.5)
    return bot.send_document(
        update.message.chat,
        "CQADAgADvAMAArCqWEsSWuzVBRHRfRYE",
        "Hicranda gonlum",
    )


def _get_main_sayfa_text(sayfa: models.Sayfa, user=None):
    text = "you've read %s sayfa of %s, Allah kabul etsin!" \
           % (sayfa.count, sayfa.book.title)

    if user and user._debug_is_me:
        text = re.sub(r' sayfa of (.*), ', f' sayfa of {sayfa.book.title} _(id: {sayfa.id})_, ', text)

    return text


def save_plain_text_sayfa(bot: Bot, update: Update, session: Session):

    raw_sayfa_value = update.message.text

    try:
        sayfa_count = int(raw_sayfa_value)
    except ValueError:
        return bot.send_message(
            chat=update.message.chat,
            text="misunderstood your sayfa value: `%s`" % raw_sayfa_value,
            parse_mode=Message.ParseMode.MARKDOWN,
        )

    user = models.User.get_or_create(session, update.message.from_)

    books = user.books or session.query(models.Book).all()

    if len(books) == 1:
        sayfa = models.Sayfa(user=user, book=books[0], count=sayfa_count, time=update.message.date)
        session.add(sayfa)
        session.commit()

        text = _get_main_sayfa_text(sayfa, user=user)

        return _user_stats(bot, update, session, user=user, with_message=text)

    markup = _books_markup(
        session,
        books=books,
        # "Lem'alar: +10s"
        text=lambda b: '{}: +{}'.format(b.title, sayfa_count),
        callback_data=lambda b: '/sayfa {} {}'.format(b.id, sayfa_count),
    )

    return bot.send_message(
        chat=update.message.chat,
        text="select book",
        reply_markup=markup,
    )


def _books_markup(session: Session, user=None, books=None, callback_data=None, text=None):
    user_books = user and user.books or []
    books = books or session.query(models.Book).options(joinedload(models.Book.author)).all()
    callback_data = callback_data or (lambda b: '{}{}'.format(BOOK_CMD, b.id))
    text = text or (lambda b: b.title + (" (✓)" if b in user_books else ""))

    markup = InlineKeyboardMarkup.from_rows_of(
        buttons=[
            InlineKeyboardMarkup.Button(
                text=text(b),
                callback_data=CALLBACK_DATA_CMD_PREFIX+callback_data(b),
            )
            for b in books
        ]
    )
    return markup


def _user_stats(bot: Bot, update: Update, session: Session, user=None, with_message=None, edit_message=None):

    message = update.message or update.callback_query.message
    chat = message.chat

    user = user or models.User.get_or_create(session, message.from_)

    ts = message.date

    user_sayfa_q = session.query(func.sum(models.Sayfa.count)).filter(
        models.Sayfa.user == user
    )

    last_day_sayfa = (
        user_sayfa_q.filter(models.Sayfa.time > ts - timedelta(days=1)).scalar() or 0
    )
    last_week_sayfa = (
        user_sayfa_q.filter(models.Sayfa.time > ts - timedelta(days=7)).scalar() or 0
    )
    last_month_sayfa = (
        user_sayfa_q.filter(models.Sayfa.time > ts - timedelta(days=30)).scalar() or 0
    )
    today_sayfa = None
    if user_day_start_time := user.settings.get_day_start_time_relative_to(ts):
        today_sayfa = user_sayfa_q.filter(models.Sayfa.time > user_day_start_time).scalar() or 0

    checkpoint = (
        session.query(models.Checkpoint).filter_by(user_id=user.id)
        .order_by(models.Checkpoint.time.desc())
        .first()
    )
    last_checkpoint_sayfa = None
    if checkpoint:
        last_checkpoint_sayfa = user_sayfa_q.filter(models.Sayfa.time > checkpoint.time).scalar() or 0

    text = _stats_text_template.render(
        custom_msg=with_message,
        today_sayfa=today_sayfa,
        last_day_sayfa=last_day_sayfa,
        last_week_sayfa=last_week_sayfa,
        last_month_sayfa=last_month_sayfa,
        last_checkpoint_sayfa=last_checkpoint_sayfa,
        last_checkpoint_time=checkpoint and checkpoint.time,
        in_user_timezone=user.settings.as_in_user_timezone,
        last_checkpoint=checkpoint,
        daily_checkpoint_time=user.settings.daily_checkpoint_time,
        timezone=user.settings.timezone,
    )

    if edit_message:
        return bot.edit_message_text(
            chat=edit_message.chat,
            message=edit_message,
            text=text,
            parse_mode=Message.ParseMode.MARKDOWN,
        )
    else:
        return bot.send_message(
            chat=chat,
            text=text,
            parse_mode=Message.ParseMode.MARKDOWN,
        )


def send_message_back(bot: Bot, update: Update):
    # bot.send_chat_action(update.message.chat, Chat.Action.TYPING)
    time.sleep(0.4)
    return bot.send_message(
        chat=update.message.chat,
        text="misunderstood: %s" % update.message.text,
    )
