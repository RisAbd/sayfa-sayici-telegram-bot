
import time
import itertools as IT, functools as FT, operator as OP  # noqa: F401, E401
from typing import Optional

from telegram import telegram
import traceback

from datetime import datetime, timedelta
import logging

import config
import attrs
import json

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import long_poll_actions


BOOK_CMD = "/sb_"
CALLBACK_DATA_CMD_PREFIX = "cmd:"

logger = logging.getLogger(__name__)
logger.setLevel(config.LOGLEVEL)


class StringifyJSONEncoder(json.JSONEncoder):
    def default(self, o):
        return f'{type(o)}: {o}'


def split_to_chunks(data, size):
    for i in range(0, len(data), size):
        yield data[i:i+size]


def _report_message(bot, text, parse_mode=telegram.Message.ParseMode.MARKDOWN, **send_msg_kwargs):
    if config.REPORT_CHAT_ID:
        chunks = split_to_chunks(text, 4096)
        for msg in chunks:
            bot.send_message(chat=config.REPORT_CHAT_ID, text=msg,
                             parse_mode=parse_mode, **send_msg_kwargs)


def main(repeating_tracebacks={}):
    # repeating_tracebacks = dict()

    engine = create_engine(config.DATABASE_URL)
    Session = sessionmaker(engine)

    if config.DEBUG:
        logging.basicConfig(level=config.LOGLEVEL)

    bot = telegram.Bot.by(token=config.BOT_API_TOKEN)

    _report_message(bot, ':: *bot restarted*')

    update: Optional[telegram.Update] = None
    while True:
        logger.debug('sleeping...')
        time.sleep(1)
        try:
            updates = bot.updates(after=update, timeout=config.GET_UPDATES_TIMEOUT)
            if not updates:
                continue
            with Session() as session:
                for update in updates:
                    if update.type == telegram.Update.Type.MESSAGE:
                        bot_command = update.message.bot_command
                    elif update.type == update.Type.CALLBACK_QUERY:
                        if not update.callback_query.data:
                            logger.warning("unknown callback_query: %r", update.callback_query)
                            continue
                        data = update.callback_query.data

                        if not data.startswith(CALLBACK_DATA_CMD_PREFIX):
                            logger.warning("unknown callback_data: %r", data)
                            print(update)
                            continue
                        bot_command = data[len(CALLBACK_DATA_CMD_PREFIX):]

                    else:
                        logger.warning("TODO: handle %r", update)
                        continue

                    if bot_command == "/audio":
                        long_poll_actions.send_audio(bot, update)
                    elif bot_command == "/books":
                        long_poll_actions.send_books_list(bot, update, session)
                    elif bot_command == "/start":
                        long_poll_actions.save_user(bot, update, session)
                    elif bot_command == "/stats":
                        long_poll_actions.user_stats(bot, update, session)
                    elif bot_command == "/sayfa":
                        long_poll_actions.user_sayfa(bot, update, session)
                    elif bot_command in ("/mybook", "/mybooks"):
                        long_poll_actions.user_books(bot, update, session)
                    elif bot_command == "/checkpoint":
                        long_poll_actions.user_checkpoint(bot, update, session)
                    elif bot_command:
                        if bot_command.startswith('/sayfa'):
                            # todo:
                            long_poll_actions.save_bot_command_sayfa(bot, update, bot_command, session)
                        elif bot_command.startswith(BOOK_CMD):
                            long_poll_actions.save_user_book(bot, update, bot_command, session)

                    elif update.message.text.strip().isdigit():
                        long_poll_actions.save_plain_text_sayfa(bot, update, session)
                    else:
                        long_poll_actions.send_message_back(bot, update)
        except KeyboardInterrupt:
            raise
        except:  # noqa: E722
            traceback.print_exc()
            if config.REPORT_CHAT_ID:
                traceback_string = traceback.format_exc()
                ts = datetime.now()
                if traceback_string not in repeating_tracebacks \
                        or ts - repeating_tracebacks[traceback_string] > timedelta(hours=1):
                    traceback.print_exc()
                    repeating_tracebacks[traceback_string] = ts
                    to_json = FT.partial(json.dumps, cls=StringifyJSONEncoder)
                    update_json = (last_update := locals().get('update')) and to_json(attrs.asdict(last_update))
                    from textwrap import dedent
                    text = dedent("""\
                        Some error occured on bot:
                        ```python
                        {traceback_string}
                        ```
                        Update serialized:
                        ```json
                        {update_json}
                        ```
                        Last response:
                        ```json
                        {last_response}
                        ```""").format(traceback_string=traceback_string,
                                       update_json=update_json,
                                       last_response=bot._last_response and to_json(bot._last_response))
                    _report_message(bot, text)
                else:
                    logger.info('not sending same traceback')
            logger.info('sleeping after error...')
            time.sleep(30)


if __name__ == '__main__':
    while True:
        try:
            main()
        except KeyboardInterrupt:
            raise
        except:  # noqa: E722
            traceback.print_exc()
            time.sleep(60)
