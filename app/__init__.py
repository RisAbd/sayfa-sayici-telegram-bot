#!/usr/bin/env python3

import logging
from urllib.parse import urlparse

from flask import Flask

import time

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from telegram import telegram
from . import config, management, models

logger = logging.getLogger(__name__)
logger.setLevel(config.LOGLEVEL)


db = SQLAlchemy(model_class=models.Base)
migrate = Migrate(db=db, render_as_batch=True)


def make_app(init_bot=None):

    app = Flask(__name__)
    app.config.from_object(config)
    app.cli.add_command(management.bootstrap)
    app.cli.add_command(management.long_poll_updates)

    app.config["SQLALCHEMY_DATABASE_URI"] = config.DATABASE_URL
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    if app.debug:
        logging.basicConfig(level=config.LOGLEVEL)

    if init_bot is None:
        init_bot = config.INIT_BOT

    bot = None
    if init_bot:
        bot = telegram.Bot.by(token=config.BOT_API_TOKEN)
        logger.debug("BOT: %r", bot)

        print(config.BOT_WEBHOOK_URL)

        if config.BOT_WEBHOOK_URL:
            target_webhook_url = config.BOT_WEBHOOK_URL
            if not urlparse(target_webhook_url).path:
                target_webhook_url += "/{}/".format(bot._api_token)

            time.sleep(1)
            webhookinfo = bot.webhookinfo()
            logger.debug("WEBHOOK_URL: %r", webhookinfo.url)

            if webhookinfo.url != target_webhook_url or config.BOT_WEBHOOK_FORCE_RESET:
                if webhookinfo:
                    # delete previous
                    time.sleep(1)
                    bot.delete_webhook()

                time.sleep(1)
                if config.BOT_CERTIFICATE_FILE:
                    with config.BOT_CERTIFICATE_FILE.open('rb') as f:
                        r = bot.set_webhook(target_webhook_url, certificate=f)
                else:
                    r = bot.set_webhook(target_webhook_url)

                logger.info("SET_WEBHOOK: %r", r)
        elif config.BOT_WEBHOOK_FORCE_RESET:
            webhookinfo = bot.webhookinfo()
            if webhookinfo:
                time.sleep(1)
                r = bot.delete_webhook()

                logger.info("UNSET_WEBHOOK: %r", r)

    db.init_app(app)
    app.db = db

    migrate.init_app(app)

    app.bot = bot

    from .webhook import bp

    app.register_blueprint(bp, url_prefix="")

    return app
