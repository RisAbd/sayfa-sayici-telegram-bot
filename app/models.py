
from enum import Enum
from datetime import datetime, time, timedelta
import typing as T
from zoneinfo import ZoneInfo

import sqlalchemy.orm
from typing_extensions import Annotated

from sqlalchemy import ForeignKey, func
from sqlalchemy.orm import DeclarativeBase, mapped_column, Mapped, relationship, joinedload


class Base(DeclarativeBase):
    def __repr__(self):
        if '__str__' in type(self).__dict__:
            s = str(self)
            return f'{type(self).__name__}{s!r}'
        return super().__repr__()


intpk = Annotated[int, mapped_column(primary_key=True)]
timestamp = Annotated[datetime, mapped_column(default=datetime.now, server_default=func.now())]


class Author(Base):
    __tablename__ = "authors"
    id: Mapped[intpk]

    name: Mapped[str]

    books: Mapped[T.List["Book"]] = relationship(back_populates="author")

    def __str__(self):
        return self.name


class Book(Base):
    __tablename__ = "books"
    id: Mapped[intpk]

    title: Mapped[str]
    year: Mapped[T.Optional[int]]

    author_id: Mapped[int] = mapped_column(ForeignKey("authors.id"))

    author: Mapped[Author] = relationship(back_populates="books")
    sayfalar: Mapped[T.List["Sayfa"]] = relationship(back_populates="book")

    users: Mapped[T.List['User']] = relationship("User", secondary="user_books",
                                                 back_populates="books")

    def __str__(self):
        return self.title


class UserBook(Base):
    __tablename__ = "user_books"
    id: Mapped[intpk]

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    book_id: Mapped[int] = mapped_column(ForeignKey("books.id"))

    add_time: Mapped[timestamp]


class User(Base):
    __tablename__ = "users"
    id: Mapped[intpk]

    first_name: Mapped[T.Optional[str]]
    last_name: Mapped[T.Optional[str]]
    username: Mapped[T.Optional[str]]
    language_code: Mapped[T.Optional[str]] = mapped_column(default='en')

    sayfalar: Mapped[T.List["Sayfa"]] = relationship(back_populates="user")
    checkpoints: Mapped[T.List["Checkpoint"]] = relationship(back_populates="user")

    books: Mapped[T.List[Book]] = relationship("Book", secondary="user_books",
                                               back_populates="users")

    settings: Mapped[T.Optional['UserSettings']] = relationship('UserSettings',
                                                                back_populates='user',
                                                                uselist=False)

    @classmethod
    def from_telegram_user(cls, user):
        return cls(
            **{
                a: getattr(user, a)
                for a in ["id", "first_name", "last_name", "username", "language_code"]
            }
        )

    @classmethod
    def get_or_create(cls, session: sqlalchemy.orm.Session, tg_user, _update=False, _flag=False):
        is_created = is_updated = False
        u = user = (session.query(User)
                    .options(joinedload(User.settings))
                    .filter(User.id == tg_user.id)
                    .first())
        if not u:
            is_created = True
            u = cls.from_telegram_user(tg_user)
        elif _update:
            for a in "last_name first_name username language_code".split():
                if getattr(user, a) != getattr(tg_user, a):
                    setattr(user, a, getattr(tg_user, a))
                    is_updated = True

        if not u.settings:
            u.settings = UserSettings()
            is_updated = True

        if is_created or is_updated:
            session.add(u)
            session.commit()
        assert u is not None, "unregistered user: %r" % u

        return u if not _flag else (u, is_created)

    @property
    def _debug_is_me(self):
        return self.id == 507902673

    @property
    def full_name(self) -> str:
        return " ".join(filter(bool, (self.first_name, self.last_name)))

    def __str__(self):
        return self.full_name


class UserSettings(Base):
    __tablename__ = 'user_settings'
    user_id: Mapped[intpk] = mapped_column(ForeignKey(User.id))

    user: Mapped[User] = relationship(User, back_populates='settings')

    class SubscriptionType(str, Enum):
        NONE = 'none'
        ALL = 'all'
        GROUP = 'group'

        def __str__(self):
            return self.value

    subscription_type: Mapped[SubscriptionType] = mapped_column(default=SubscriptionType.ALL)
    language = 'en'  # todo:

    timezone: Mapped[T.Optional[str]]
    daily_checkpoint_time: Mapped[time] = mapped_column(default=time(0, 0))

    def as_in_user_timezone(self, ts: datetime) -> datetime:
        if self.timezone:
            ts.astimezone(ZoneInfo(self.timezone))
        return ts

    def get_day_start_time_relative_to(self, ts: datetime) -> T.Optional[datetime]:
        if self.timezone and self.daily_checkpoint_time:
            ts = self.as_in_user_timezone(ts)
            dch_time = self.daily_checkpoint_time
            user_day_start_time = ts.replace(hour=dch_time.hour, minute=dch_time.minute,
                                             second=dch_time.second, microsecond=dch_time.microsecond)
            if ts.time() < dch_time:
                user_day_start_time -= timedelta(days=1)
            return user_day_start_time

    def __str__(self):
        d = dict(self.__dict__)
        d.pop('user', None)
        d.pop('_sa_instance_state')
        return str(d)


class Sayfa(Base):

    __tablename__ = 'sayfa'

    id: Mapped[intpk]
    count: Mapped[int]
    time: Mapped[timestamp]

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    book_id: Mapped[int] = mapped_column(ForeignKey("books.id"))

    user: Mapped[User] = relationship(back_populates="sayfalar")
    book: Mapped[Book] = relationship(back_populates="sayfalar")

    def __str__(self):
        return str(self.count)


class Checkpoint(Base):

    __tablename__ = "checkpoints"

    id: Mapped[intpk]
    name: Mapped[T.Optional[str]]
    time: Mapped[timestamp]

    user_id: Mapped[int] = mapped_column(ForeignKey("users.id", ondelete="CASCADE"))

    user: Mapped[User] = relationship(back_populates="checkpoints")

    def __str__(self):
        return (self.name and self.name + ", " or "") + self.time.strftime(
            "%d/%m/%Y %H:%M:%S"
        )
