"""empty message

Revision ID: 7de9814b9313
Revises: bace615d47b5
Create Date: 2020-12-21 23:39:51.335724

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7de9814b9313'
down_revision = 'bace615d47b5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user_books',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('book_id', sa.Integer(), nullable=True),
    sa.Column('add_time', sa.DateTime(), server_default=sa.text('(CURRENT_TIMESTAMP)'), nullable=False),
    sa.ForeignKeyConstraint(['book_id'], ['books.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )

    # existing favorite books migration
    op.execute('''
INSERT INTO user_books (user_id, book_id)
    SELECT id, book_id FROM users
        WHERE book_id IS NOT NULL
''')
    

    # op.drop_constraint(None, 'users', type_='foreignkey')
    # op.drop_column('users', 'book_id')
    
    with op.batch_alter_table('users', schema=None) as batch_op:
        # batch_op.drop_constraint(None, type_='foreignkey')
        batch_op.drop_column('book_id')
    
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###

    # op.add_column('users', sa.Column('book_id', sa.INTEGER(), nullable=True))
    # op.create_foreign_key(None, 'users', 'books', ['book_id'], ['id'])
    
    with op.batch_alter_table('users', schema=None) as batch_op:
        batch_op.add_column(sa.Column('book_id', sa.INTEGER(), nullable=True))
        batch_op.create_foreign_key('fk_users_book_id_books', 'books', ['book_id'], ['id'])

    # trying to set one of favorite books as user's favorite book in old scheme
    op.execute('''
UPDATE users AS u
    SET book_id = (SELECT ub.book_id 
                   FROM user_books AS ub 
                   WHERE u.id = ub.user_id
                   ORDER BY add_time
                   LIMIT 1)
''')

    op.drop_table('user_books')
    # ### end Alembic commands ###
