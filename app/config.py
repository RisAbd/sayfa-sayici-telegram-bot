
from decouple import config
import logging
from pathlib import Path

INIT_BOT = config('INIT_BOT', cast=bool, default=True)

DEBUG = config('DEBUG', cast=bool, default=config('FLASK_DEBUG', cast=bool, default=False))

BOT_API_TOKEN = config("BOT_API_TOKEN", cast=str)
BOT_WEBHOOK_URL = config("BOT_WEBHOOK_URL", cast=str, default='')
BOT_CERTIFICATE_FILE = config(
    "BOT_CERTIFICATE_FILE", cast=lambda v: None if v is None else Path(v), default=None
)
BOT_WEBHOOK_FORCE_RESET = config('BOT_WEBHOOK_FORCE_RESET', cast=bool, default=False)

RESOURCES_DIRECTORY = config("RESOURCES_DIRECTORY", default=None)
if RESOURCES_DIRECTORY is None:
    RESOURCES_DIRECTORY = (Path(__file__).parent.parent / 'resources')
RESOURCES_DIRECTORY = Path(RESOURCES_DIRECTORY).absolute()

_DEFAULT_DATABASE_URL = "sqlite:///" + str(RESOURCES_DIRECTORY / "data.db")

DATABASE_URL = config("DATABASE_URL", cast=str, default=_DEFAULT_DATABASE_URL)

_LOGLEVEL = config("LOGLEVEL", default="INFO", cast=str)
LOGLEVEL = getattr(logging, _LOGLEVEL)

GET_UPDATES_TIMEOUT = config('GET_UPDATES_TIMEOUT', cast=int, default=60)

REPORT_CHAT_ID = config('REPORT_ERRORS_CHAT_ID', cast=int, default=507902673)


PORT = config("PORT", cast=int, default=5000)
HOST = config("HOST", cast=str, default="0.0.0.0")
